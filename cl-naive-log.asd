(defsystem "cl-naive-log"
  :description "A simple log system."
  :version "2023.10.7"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:bordeaux-threads)
  :components (
	       (:file "src/packages")
	       (:file "src/log"                :depends-on ("src/packages"))))
                                                       
