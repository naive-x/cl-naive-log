(in-package :cl-naive-log.tests)

(defun ensure-broadcast-stream-lists (stream)
  (etypecase stream
    (broadcast-stream (broadcast-stream-streams stream))
    (stream (list stream))
    (null '())))

(defun open-log-file (stem)
  (open (format nil "/tmp/cl-naive-log--~A--~A.txt"
                (osicat-posix:getpid)
                stem)
        :element-type 'character
        :direction :output
        :if-exists :supersede
        :if-does-not-exist :create))

;; From stream.lisp (to be stand alone):

(defun contents-from-stream (stream &key length (min-size 256) max-extend)
  "
STREAM:     May be a binary or character, file or non-file stream.
LENGTH:     NIL, or the number of stream elements to read.
MIN-SIZE:   Minimum pre-allocated buffer size. If LENGTH is given, or STREAM
            has a FILE-LENGTH, then the MIN-SIZE is ignored.
MAX-EXTEND: NIL ==> double the buffer size, or double the buffer size until
            it's greater than MAX-EXTEND, and then increment by MAX-EXTEND.
RETURN:     A vector containing the elements read from the STREAM.
"
  (let* ((busize (or length (ignore-errors (file-length stream)) min-size))
         (eltype (stream-element-type stream))
         (initel (if (subtypep eltype 'integer) 0 #\Space))
         (buffer (make-array busize
                             :element-type eltype
                             :initial-element initel
                             :adjustable t :fill-pointer t))
         (start 0))
    (loop
       (let ((end (read-sequence buffer stream :start start)))
         (when (or (< end busize) (and length (= length end)))
           ;; we got eof, or have read enough
           (setf (fill-pointer buffer) end)
           (return-from contents-from-stream buffer))
         ;; no eof; extend the buffer
         (setf busize
               (if (or (null max-extend) (<= (* 2 busize) max-extend))
                   (* 2 busize)
                   (+ busize max-extend))
               start end))
       (adjust-array buffer busize :initial-element initel :fill-pointer t))))


;; From file.lisp (to be stand alone):

(defun text-file-contents (path &key (if-does-not-exist :error)
                           (external-format :default))
  "
RETURN: The contents of the file at PATH as a LIST of STRING lines.
        or what is specified by IF-DOES-NOT-EXIST if it doesn't exist.
"
  (with-open-file (in path :direction :input
                      :if-does-not-exist if-does-not-exist
                      :external-format external-format)
    (if (and (streamp in) (not (eq in if-does-not-exist)))
        (contents-from-stream in :min-size 16384)
        in)))

;;;
;;; log-filter
;;;

(defmethod log-filter (level (object number))
  nil)

;;;
;;; log-stream
;;;

(defmethod log-stream (level (object (eql :none)))
  (declare (ignorable level object))
  nil)

(defvar *test-log-file-stream*   nil)
(defvar *test-log-string-stream* nil)
(defvar *test-log-both-stream*   nil)

(defmethod log-stream (level (object (eql :file-stream)))
  (declare (ignorable level object))
  (or *test-log-file-stream*
      (setf *test-log-file-stream* (open-log-file "test-file-0"))))

(defmethod log-stream (level (object (eql :string-stream)))
  (declare (ignorable level object))
  (or *test-log-string-stream*
      (setf *test-log-string-stream*
            (make-string-output-stream :element-type 'character))))

(defmethod log-stream (level (object (eql :both-stream)))
  (declare (ignorable level object))
  (or *test-log-both-stream*
      (setf *test-log-both-stream*
            (make-broadcast-stream (log-stream level :file-stream)
                                   (log-stream level :string-stream)))))


(defmacro with-log-stream (&body body)
  `(let ((*test-log-file-stream*   nil)
         (*test-log-string-stream* nil)
         (*test-log-both-stream*   nil))
     (block nil
       ,@body
       (list (when *test-log-file-stream*
               (text-file-contents (namestring *test-log-file-stream*)))
             (when *test-log-string-stream*
               (get-output-stream-string *test-log-string-stream*))))))

(testsuite minimal-log-level-p
  (testcase minimal-log-level-p/info
            :expected '(nil nil t t t t)
            :actual (let ((*log-levels* '(:debug :info :warn :error :fatal))
                          (*minimal-log-level*   :info))
                      (mapcar (function minimal-log-level-p)
                              (cons :unknown *log-levels*))))
  (testcase minimal-log-level-p/fatal
            :expected '(nil nil nil nil nil t)
            :actual (let ((*log-levels* '(:debugx :infox :warnx :errorx :fatalx))
                          (*minimal-log-level*   :fatalx))
                      (mapcar (function minimal-log-level-p)
                              (cons :unknown *log-levels*))))
  (testcase minimal-log-level-p/bad
            :expected '(nil nil nil nil nil nil)
            :actual (let ((*log-levels* '(:debug :info :warn :error :fatal))
                          (*minimal-log-level*   :bad))
                      (mapcar (function minimal-log-level-p)
                              (cons :unknown *log-levels*))))

  (testcase log-filter/info
            :expected '(nil nil t t t t)
            :actual (let ((*log-levels* '(:debug :info :warn :error :fatal))
                          (*minimal-log-level*   :info))
                      (mapcar (lambda (level) (log-filter level "dummy object"))
                              (cons :unknown *log-levels*))))
  (testcase log-filter/fatal
            :expected '(nil nil nil nil nil t)
            :actual (let ((*log-levels* '(:debugx :infox :warnx :errorx :fatalx))
                          (*minimal-log-level*   :fatalx))
                      (mapcar (lambda (level) (log-filter level "dummy object"))
                              (cons :unknown *log-levels*))))
  (testcase log-filter/bad
            :expected '(nil nil nil nil nil nil)
            :actual (let ((*log-levels* '(:debug :info :warn :error :fatal))
                          (*minimal-log-level*   :bad))
                      (mapcar (lambda (level) (log-filter level "dummy object"))
                              (cons :unknown *log-levels*))))

  (testcase log-filter/info/all-nil
            :expected '(nil nil nil nil nil nil)
            :actual (let ((*log-levels* '(:debug :info :warn :error :fatal))
                          (*minimal-log-level*   :info))
                      (mapcar (lambda (level) (log-filter level 42))
                              (cons :unknown *log-levels*))))

  );;testsuite


(testsuite split-broadcast-stream

  (testcase split-broadcast-stream/check-both
            :expected '((t t) 2 3)
            :actual (let* ((a (open-log-file "test-file-1"))
                           (b (open-log-file "test-file-2"))
                           (both (make-broadcast-stream
                                  a b
                                  *trace-output*
                                  *standard-output*
                                  *error-output*)))
                      (unwind-protect
                           (multiple-value-bind (file-streams other-streams) (split-broadcast-stream both)
                             (list (mapcar (lambda (stream)
                                             (when stream (not (not (search "test-file-" (namestring stream))))))
                                           (ensure-broadcast-stream-lists file-streams))
                                   (length  (ensure-broadcast-stream-lists file-streams))
                                   (length  (ensure-broadcast-stream-lists other-streams))))
                        (close a)
                        (close b)))
            :equal 'equal)

  (testcase split-broadcast-stream/only-file-stream
            :expected '((t) 1 0)
            :actual (let* ((stream (open-log-file "test-file-1")))
                      (unwind-protect
                           (multiple-value-bind (file-streams other-streams) (split-broadcast-stream stream)
                             (list (mapcar (lambda (stream)
                                             (when stream (not (not (search "test-file-" (namestring stream))))))
                                           (ensure-broadcast-stream-lists file-streams))
                                   (length  (ensure-broadcast-stream-lists file-streams))
                                   (length  (ensure-broadcast-stream-lists other-streams))))
                        (close stream)))
            :equal 'equal)

  (testcase split-broadcast-stream/only-other-stream
            :expected '(() 0 2)
            :actual (let ((both (make-broadcast-stream
                                 *standard-output*
                                 *error-output*)))
                      (multiple-value-bind (file-streams other-streams) (split-broadcast-stream both)
                        (list (mapcar (lambda (stream)
                                        (when stream (not (not (search "test-file-" (namestring stream))))))
                                      (ensure-broadcast-stream-lists file-streams))
                              (length  (ensure-broadcast-stream-lists file-streams))
                              (length  (ensure-broadcast-stream-lists other-streams)))))
            :equal 'equal)

  ) ;;testsuite  


(testsuite log-stream

  ;; TODO: add *log-functions*
  
  (testcase log-stream/none
            :expected '(nil nil)
            :actual (with-log-stream
                      (log-message :info :none "Foo")))

  (testcase log-stream/file-stream
            :expected '(t nil)
            :actual (destructuring-bind (fo so)
                        (with-log-stream
                          (log-message :info :file-stream "Foo"))
                      (list (not (not (search "INFO :FILE-STREAM Foo" fo)))
                            so)))

  (testcase log-stream/string-stream
            :expected '(nil t)
            :actual (destructuring-bind (fo so)
                        (with-log-stream
                          (log-message :info :string-stream "Foo"))
                      (list fo
                            (not (not (search "INFO :STRING-STREAM Foo" so))))))

  (testcase log-stream/both
            :expected '(t t)
            :actual  (destructuring-bind (fo so)
                         (with-log-stream
                           (log-message :info :both-stream "Foo"))
                       (list (not (not (search "INFO :BOTH-STREAM Foo" fo)))
                             (not (not (search "INFO :BOTH-STREAM Foo" so))))))
  ) ;;testsuite
