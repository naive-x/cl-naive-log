all: test documentation examples
.PHONY:: test tests documentation examples APIs API APIS api apis

# default values:
ARTDIR = tests/artifacts/
DEPENDENCYDIR = $(abspath $(CURDIR)/..)/
THISDIR = $(abspath $(CURDIR))/

test tests: APIs
	sbcl --noinform --no-userinit --non-interactive \
		--eval '(proclaim (quote (optimize (safety 3) (debug 3) (space 0) (speed 0) (compilation-speed 3))))' \
		--eval '(load #P"~/quicklisp/setup.lisp")' \
		--eval '(push "$(DEPENDENCYDIR)" ql:*local-project-directories*)' \
		--eval '(push #P"$(THISDIR)" asdf:*central-registry*)' \
		--eval '(ql:quickload :cl-naive-log.tests)' \
		--eval '(setf cl-naive-tests:*verbose* :trace)' \
		--eval '(write-line "before")' --eval '(force-output)' \
		--eval '(cl-naive-tests:run :debug nil)' \
		--eval '(write-line "mid")' --eval '(force-output)' \
		--eval '(cl-naive-tests:run :debug nil)' \
		--eval '(write-line "after")' --eval '(force-output)' \
		--eval '(cl-naive-tests:write-results cl-naive-tests:*suites-results* :format :text)' \
		--eval '(cl-naive-tests:save-results cl-naive-tests:*suites-results* :file "$(ARTDIR)junit-results.xml" :format :junit)' \
		--eval '(sb-ext:exit :code (if (cl-naive-tests:report) 0 200))'
documentation:
	make -C docs pdfs


EXAMPLES = log
examples:APIs
	@ for d in $(EXAMPLES) ; do echo ===== $$(pwd) ===== ; make -C examples/$$d all ; done


clean:
	@ make -C docs clean
	@ for d in  $(EXAMPLES) ; do make -C examples/$$d clean ; done
	@ find . \( -name \*.fasl -o -name \*.dx64fsl \) -exec rm {} +

FMT = "make %-20s \# %s\n"
help:
	@ printf $(FMT) help                'Print this help.'
	@ printf $(FMT) tests               'Run the unit tests.'
	@ printf $(FMT) documentation       'Build the documentation.'
	@ printf $(FMT) examples            'Build the examples.'
	@ printf $(FMT) clean               'Remove old products.'
