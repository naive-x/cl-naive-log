(defsystem "cl-naive-log.tests"
  :description "Tests the simple log system."
  :version "2023.10.22"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-naive-log
               :cl-naive-tests
               :osicat)
  :components (
	       (:file "tests/packages")
	       (:file "tests/unit-test" :depends-on ("tests/packages"))))
                                                       
