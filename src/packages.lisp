(defpackage :cl-naive-log
  (:use :common-lisp)
  (:export
   #:*log-levels*
   #:*log-colors*
   #:*minimal-log-level*
   #:*log-functions*
   #:*default-log-file-pathname*
   #:*log-file-pathname*
   #:log-filter
   #:log-stream
   #:printable
   #:format-log-message
   #:log-message
   #:sgr
   #:iso-timestamp
   #:minimal-log-level-p
   #:log-file-pathname
   #:file-stream-p
   #:split-broadcast-stream
   #:ensure-log-stream
   #:close-log-files
   #:with-io-syntax))
