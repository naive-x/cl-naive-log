(in-package :cl-naive-log)

(defvar *debugging* nil
  "Set to true to invoke the debugger upon formatting errors.")

(defgeneric log-filter (level object)
  (:documentation "Client may define methods on this generic function
to implement specific log filtering.
RETURN: NIL to prevent writing the log message.
The default implementation filters according to the level."))

(defgeneric log-stream (level object)
  (:documentation "Client may define methods on this generic function
to direct the output to specific streams or file streams.
Note: this allows to select different files for different logging objects.
The default implementation writes to *TRACE-OUTPUT* and the *LOG-FILE*.
\(Using a BROADCAST-STREAM, but with special handling
in case of #+use-ansi-colors)."))

(defgeneric printable (object)
  (:documentation "Return a printable form of the object.
Preferably a string, but it can be a printable-readably sexp."))

(defgeneric format-log-message (level object format-control format-arguments)
  (:documentation "Clients may define methods on this generic function
to format the actual log message.  It should include a time-stamp and the OBJECT,
in addition to the formatted format-control and format-arguments.
It should preferably be one single line (without newlines).
If multi-line, then the last line shouldn't have a newline.
Must not signal any ERROR."))

(defgeneric log-message (level object format-control &rest format-arguments)
  (:documentation "Writes a formatted log message to the (LOG-STREAM LEVEL OBJECT)
if (LOG-FILTER LEVEL OBJECT) allows it."))

(defvar *minimal-log-level* :info
  "The minimal log level to log. A keyword from *LOG-LEVELS*")
(defvar *log-levels* '(:debug :info :warn :error :fatal)
  "The log levels in order of severity.  A list of keywords.
The client may bind its own list of log level keywords.
See *LOG-COLORS*")

(defun sgr (&rest ps...)
  "SELECT GRAPHIC RENDITION"
  #-use-ansi-colors (declare (ignore ps...))
  #+use-ansi-colors
  (map 'string #'code-char (append '(27 91)
                                   (map 'list #'char-code
                                        (format nil "~{~D~^;~}" ps...))
                                   '(109)))
  #-use-ansi-colors "")

(defvar *log-colors*
  #+use-ansi-colors (list (sgr 35)      ; magenta - debug
                          (sgr 32)      ; green   - info
                          (sgr 33)      ; yellow  - warning
                          (sgr 31)      ; red     - error
                          (sgr 31 1 4)) ; red bold underline - fatal
  #-use-ansi-colors (list "" "" "" "" "")
  "The ANSI escape sequences to use for the log levels. A list of strings.
This list must be at least of the length of *LOG-LEVELS*.")

(defvar *log-functions* '()
  "A list of functions to call in addition to writing the message
to the (LOG-STREAM LEVEL OBJECT).
They are called with the log level, the object, and the formatted message.
They should not signal an error; if one does, the error message is logged,
and the function is removed from the *LOG-FUNCTIONS*.")




(defvar *last-time* (cons 0 "00000000T000000Z")
  "A cache of the last formatted iso-timestamp.")
(defun iso-timestamp (&optional (time (get-universal-time)))
  "Return the ISO-8859 formatted TIMESTAMP"
  (if (= time (car *last-time*))
      (cdr *last-time*)
      (setf (car *last-time*) time
            (cdr *last-time*)
            (multiple-value-bind (se mi ho da mo ye) (decode-universal-time time 0)
              (format nil "~4,'0D~2,'0D~2,'0DT~2,'0D~2,'0D~2,'0DZ"
                      ye mo da ho mi se)))))

(defvar *log-cached-level-width*  nil
  "A Cache of the maximum width of the log levels in characters, used for formatting.
See: LOG-LEVEL-WIDTH")

(defun log-level-width ()
  "
RETURNS: The maximum width of the log levels in characters.
"
  (cdr (if (eq (car *log-cached-level-width*) *log-levels*)
           *log-cached-level-width*
           (setf *log-cached-level-width*
                 (cons *log-levels*
                       (reduce (function max) *log-levels*
                               :initial-value 0
                               :key (lambda (x) (length (symbol-name x)))))))))

(defvar *log-cached-minimal-level-index* (cons (cons nil nil) nil)
  "Cache for the index of the minimal log level. See MINIMAL-LOG-LEVEL-P.")
(defvar *log-cached-last-level-index*    (cons nil nil)
  "Cache for the index of the last log level. See MINIMAL-LOG-LEVEL-P.")

(defun minimal-log-level-p (level)
  "
LEVEL:   one of the keywords in *LOG-LEVELS*.
RETURNS: whether LEVEL is greater than or equal to the minimal log level.
NOTE:    if LEVEL is not *LOG-LEVELS* then NIL is returned.
"
  (<= (if (and (eql (caar *log-cached-minimal-level-index*)
                    *minimal-log-level*)
               (eql (cdar *log-cached-minimal-level-index*)
                    *log-levels*))
          (cdr *log-cached-minimal-level-index*)
          (setf (caar *log-cached-minimal-level-index*) *minimal-log-level*
                (cdar *log-cached-minimal-level-index*) *log-levels*
                (cdr *log-cached-minimal-level-index*)
                (or (position *minimal-log-level* *log-levels* :test #'equal)
                    (length *log-levels*))))
      (if (eql (car *log-cached-last-level-index*)               
               level)
          (cdr *log-cached-last-level-index*)
          (setf (car *log-cached-last-level-index*) level
                (cdr *log-cached-last-level-index*)
                (or (position level *log-levels* :test #'equal)
                    0)))))




(defvar *log-lock*           (bt:make-lock "*log-lock*"))
(defvar *log-functions-lock* (bt:make-lock "*log-functions-lock*"))



(defvar *default-log-file-pathname*
  (make-pathname :name "application"
                 :type "log"
                 :directory '(:absolute "tmp"))
  "The defaults for the log file pathnames.
This pathname will be merged with the specific pathname components.")

(defvar *log-file-pathname*
  (make-pathname :name "application")
  "The log file pathname.
This pathname will be merged with the *DEFAULT-LOG-FILE-PATHNAME* components.")

(defgeneric log-file-pathname (level object)
  (:documentation "The pathname of the log file.  Set it to some nice place.
The default method returns *DEFAULT-LOG-FILE-PATHNAME*.")
  (:method (level object)
    (declare (ignorable level object))
    (merge-pathnames *log-file-pathname* *default-log-file-pathname* nil)))


(defun file-stream-p (stream)
  "Return whether stream is a FILE-STREAM."
  (typep stream 'file-stream))

(defun split-broadcast-stream (stream)
  "
If stream is a file-stream, returns (values stream nil)
If stream is a broadcast-stream, returns (values fs os)
with fs either NIL, a file-stream or a broadcast stream containing only file-streams,
and os  either NIL, a non-file-stream or a broadcast stream containing only non-file-streams.
If stream is another type of stream, then (values nil stream).
"
  (etypecase stream
    (broadcast-stream
     (loop :for stream :in  (broadcast-stream-streams stream)
           :if (file-stream-p stream)
           :collect stream :into file-streams
           :else
           :collect stream :into terminal-streams
           :finally (return (values (if (cdr file-streams)
                                        (apply (function make-broadcast-stream) file-streams)
                                        (car file-streams))
                                    (if (cdr terminal-streams)
                                        (apply (function make-broadcast-stream) terminal-streams)
                                        (car terminal-streams))))))
    (file-stream
     (values stream nil))
    (stream
     (values nil stream))
    (null
     (values nil nil))))

(defvar *log-stream* nil)

(defgeneric ensure-log-stream (level object)
  (:documentation "Sets and returns *LOG-STREAM*."))

(defmethod ensure-log-stream (level object)
  (unless *log-stream*
    (setf *log-stream*
          (make-broadcast-stream *trace-output*
                                 (open (log-file-pathname level object)
                                       :direction :output
                                       :if-does-not-exist :create
                                       :if-exists :append
                                       #+ccl :sharing #+ccl :external))))
  *log-stream*)

(defun close-log-files ()
  "Closes the files of the *LOG-STREAM* broadcast-stream,
and sets it to *TRACE-OUTPUT*."
  (bt:with-lock-held (*log-lock*)
    (multiple-value-bind (file-stream terminal-stream)
        (split-broadcast-stream *log-stream*)
      (declare (ignore terminal-stream))
      (when file-stream
        (close file-stream))
      (setf *log-stream* *trace-output*))))

(defmethod log-stream (level object)
  ;;
  ;; Client could override this method to dispatch to
  ;; different files depending on the level or the object.
  ;; But this default implementation simply writes to a single file.
  ;; 
  (bt:with-lock-held (*log-lock*)
    (ensure-log-stream level object)))




(defmethod log-filter (level object)
  (declare (ignore object))
  (minimal-log-level-p level))

(defmacro with-io-syntax (&body body)
  `(with-standard-io-syntax
     (let ((*read-default-float-format* 'double-float)
           (*print-readably* nil))
       ,@body)))

(defmethod format-log-message (level object format-control format-arguments)
  (with-io-syntax
    (let ((stamp (iso-timestamp)))
      (handler-case
          (format nil "~A ~:@(~VA~) ~@[~A ~]~?"
                  stamp
                  *log-cached-level-width* level
                  (printable object)
                  format-control format-arguments)
        (error ()
          (handler-case
              (format nil "~A ~:@(~VA~) ~@[~A ~]~A ~A"
                      stamp
                      *log-cached-level-width* level
                      (printable object)
                      format-control format-arguments)
            (error (err)
              (when *DEBUGGING*
                (invoke-debugger err))
              (format nil "~A ~:@(~VA~) FORMAT ERROR WHILE LOGGING MESSAGE ~S"
                      stamp
                      *log-cached-level-width* :ERROR
                      format-control))))))))


(defmethod printable (object)
  (with-io-syntax
    (prin1-to-string object)))

(defmethod log-message (level object format-control &rest format-arguments)
  "Write a log message to the (LOG-STREAM LEVEL OBJECT) if (LOG-FILTER LEVEL OBJECT) allows it.

The message is formatted and written only if (LOG-FILTER LEVEL OBJECT)
returns true. (Default is only if the level is greater than
or equal to the minimal log level. See: *LOG-LEVELS* and *MINIMAL-LOG-LEVEL*).

If the level is :ERROR or :FATAL then the message is also written to
*ERROR-OUTPUT* (if it's not already in the (LOG-STREAM LEVEL OBJECT)).

If *LOG-FUNCTIONS* is bound to a list of functions, then they are
called with the log level, the object, and the formatted message.  If
any signals an error, it's logged and it's removed from the
*LOG-FUNCTIONS* list.

RETURN: the formatted log message.
"
  (when (log-filter level object)
    (let ((message (format-log-message level object format-control format-arguments)))
      (multiple-value-bind (file-stream terminal-stream)
          (split-broadcast-stream (log-stream level object))

        (when file-stream
          (bt:with-lock-held (*log-lock*)
            (write-line message file-stream)
            (force-output file-stream)))

        (when terminal-stream
          (bt:with-lock-held (*log-lock*)
            (fresh-line terminal-stream)
            #+use-ansi-colors
            (write-string (nth (position level *log-levels*) *log-colors*) terminal-stream)
            (write-string message terminal-stream)
            #+use-ansi-colors (write-string (sgr 0) terminal-stream)
            (terpri terminal-stream)
            (force-output terminal-stream)))

        (when (and (member level '(:error :fatal))
                   (not (member *error-output*
                                (typecase terminal-stream
                                  (null             '())
                                  (broadcast-stream (broadcast-stream-streams terminal-stream))
                                  (t                (list terminal-stream))))))
          (bt:with-lock-held (*log-lock*)
            (fresh-line *error-output*)
            (write-line message *error-output*)
            (force-output *error-output*)))

        (when *log-functions*
          (bt:with-lock-held (*log-functions-lock*)
            (let ((bad-log-functions '()))
              (dolist (log-function *log-functions*)
                (handler-case (funcall log-function level object message)
                  (error (err)
                    (push log-function bad-log-functions)
                    (log-message :error log-function
                                 "log-function signaled the error: ~A ; it's removed."
                                 err))))
              (when bad-log-functions
                (setf *log-functions* (delete-if (lambda (fun) (member fun bad-log-functions)) *log-functions*))))))
        
        message))))


;;;; THE END ;;;;

