(in-package :hunchentoot)

;;
;; This file overrides the HUNCHENTOOT:ISO-TIME function to use the same format as CL-NAIVE-LOG. 
;;

(defun iso-time (&optional (time (get-universal-time)))
  "Returns the universal time TIME as a string in full ISO format."
  (cl-naive-log:iso-timestamp time))

;;
;; TODO: something about hunchentoot accessor log destinations:
;;
;;    (access-log-destination :initarg :access-log-destination
;;                         :accessor acceptor-access-log-destination
;;                         :documentation "Destination of the access log
;; which contains one log entry per request handled in a format similar
;; to Apache's access.log.  Can be set to a pathname or string
;; designating the log file, to a open output stream or to NIL to
;; suppress logging.")
;;    (message-log-destination :initarg :message-log-destination
;;                          :accessor acceptor-message-log-destination
;;                          :documentation "Destination of the server
;; error log which is used to log informational, warning and error
;; messages in a free-text format intended for human inspection. Can be
;; set to a pathname or string designating the log file, to a open output
;; stream or to NIL to suppress logging.")
