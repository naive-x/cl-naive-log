(defpackage :cl-naive-log.examples.minimal-example
  (:use :cl :cl-naive-log)
  (:export :main))
(in-package :cl-naive-log.examples.minimal-example)

(defclass helper ()
  ())

(defmethod printable ((helper helper))
  (with-output-to-string (stream)
    (print-unreadable-object (helper stream :type nil :identity t)
      (write-string "HELPER" stream))))

(defmethod run ((helper helper) (argument null))
  (log-message :error helper "Helper must not be called with NIL!"))

(defmethod run ((helper helper) argument)
  (log-message :info helper "Helper is called with ~A" argument))

(defun main ()
  (setf *log-file-pathname* #P"/tmp/minimal-example--errors.log"
        *minimal-log-level* :error)
  (cl-naive-log:log-message :info 'main "Program started.")
  (run (make-instance 'helper) 42)
  (run (make-instance 'helper) nil)
  (run (make-instance 'helper) 33)
  0)
